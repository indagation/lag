<?php
/*
Template Name: Logged-In Users Page
*/
?>

<?php get_header(); ?>

<?php
	/** 
	 * cleanretina_before_main_container hook
	 */
	do_action( 'cleanretina_before_main_container' );
?>

<div id="container">
<?php if(is_user_logged_in()):?>
	<?php
		/** 
		 * cleanretina_main_container hook
		 *
		 * HOOKED_FUNCTION_NAME PRIORITY
		 *
		 * cleanretina_content 10
		 */
		do_action( 'cleanretina_main_container' );
	?>
</div><!-- #container -->

<?php
	/** 
	 * cleanretina_after_main_container hook
	 */
	do_action( 'cleanretina_after_main_container' );
?>

<?php get_footer(); ?>